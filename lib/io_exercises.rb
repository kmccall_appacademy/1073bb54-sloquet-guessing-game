# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.



def guessing_game
  correct = rand(100) + 1
  number_of_guesses = 0
  while true
    number_of_guesses += 1
    puts "guess a number"
    guess = gets.chomp.to_i
    puts guess
    puts "Number of Guesses: #{number_of_guesses}"
    puts higher_or_lower(correct, guess)
    return "The number was #{correct}" if correct == guess
  end
end

def higher_or_lower(correct, guess)
  correct > guess ? "too low" : "too high"
end

def file_shuffle
  puts "what file would you like to shuffle"
  file_name = gets.chomp
  shuffled_content = File.readlines(file_name).shuffle
  File.open("#{file_name}-shuffled.txt", "w") do |file|
    file.puts shuffled_content
  end
end

#file_shuffle
